import { user, category, project } from '../helper';

describe('User', () => {
  test('registery', async () => {
    const data = {
      email: 'test@test.ru',
      password: 'testtest'
    }

    const req = await user.registery(data)

    expect(res.data).toEqual(data)
  })

  // test('login', () => {
  //   const data = {
  //     email: 'qwerty@qwerty.ru',
  //     password: 'qwerty'
  //   }

  //   user.login(data).then(res => {
  //     expect(res.data).toEqual(data)
  //   })
  // })

  test('data', () => {
    const data = {
      id: 1,
      email: "1@twtt.com"
    }

    return user.getUserData(1).then(res => {
      expect(res.data).toEqual(data)
    })
    
  })
})

describe('Category', () => {
  test('create', () => {
    const data = {
      category: 'Test'
    }

    category.addCategory(data).then(res => {
      expect(res.data).toEqual(data)
    })
  })

  test('all category', () => {
    category.getAllCategory().then(res => {
      expect(res.data).toBeInstanceOf(Array)
    })
  })

  test('data select category', () => {
    const data = {
      id: 1,
      category: "Медицина"
    }

    category.getSelectCategory(1).then(res => {
      expect(res.data).toEqual(data)
    })
  })
})

describe('Project', () => {
  const data = {
    name: "Tesn",
    email: "test@test.com",
    inn: "123456789876",
    link: "test.com",
    title: "Test Project",
    text: "Unit test priject",
    category: 1
  }

  test('create', () => {
    project.addProject(data).then(res => {
      expect(res.data).toBeInstanceOf(Object)
    })
  })

  test('all progect', () => {
    category.getAllCategory().then(res => {
      expect(res.data).toBeInstanceOf(Array)
    })
  })

  // test('data select project', () => {
  //   category.getSelectCategory(1).then(res => {
  //     expect(res.data.id).toEqual()
  //   })
  // })
})


