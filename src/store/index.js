import Vue from 'vue'
import Vuex from 'vuex'

import categories from './modules/categories.js'
import projects from './modules/projects.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    categories,
    projects
  }
})
