import { category } from "@/modules/helper";

export default {
  actions: {
    async laodCategoriesList(ctx) {
      const data = await category.getAllCategory()

      ctx.commit("setCategoriesData", data.data)
    }
  },
  mutations: {
      setCategoriesData(state, data) {
      state.categories = data;
    }
  },
  state: {
    categories: []
  },
  getters: {
    getCategories(state) {
      return state.categories;
    }
  }
}