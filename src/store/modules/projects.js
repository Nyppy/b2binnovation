import { project } from "@/modules/helper";

export default {
  actions: {
    async laodProjectsList(ctx, list) {
      const data = await project.getAllProjects()
      
      let listData = data.data;
      listData.forEach(el => {
        el.show = true;
      });

      ctx.commit("setProjectsData", list ? list : listData)
    }
  },
  mutations: {
    setProjectsData(state, data) {
      state.projects = data;
    }
  },
  state: {
    projects: []
  },
  getters: {
    getProjects(state) {
      return state.projects;
    }
  }
}